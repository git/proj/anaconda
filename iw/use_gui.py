#
#use_gui.py: gui USE flags selection.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui
import subprocess
import re
import shutil

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class UseWindow(InstallWindow):
	
	buttons = []
	use_profile = []
	
	def getNext(self):
		use = []
		for button in self.buttons:
			if button.get_property("active"):
				use.append(button.get_property("label"))
				
		for flag in use:
			if flag not in self.use_profile: # flag enabled by user, disabled by profile
				self.anaconda.use_flags.append(flag)
		for flag in self.use_profile:
			if flag not in use: # flag is disabled by user, enabled by profile
				self.anaconda.use_flags.append("-"+flag)
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf        

		(self.xml, self.align) = gui.getGladeWidget("use.glade", "use_align")
		
		# Temporarily move the make.conf file and query emerge to find out the flags selected by the profile
		shutil.move("/etc/make.conf", "/etc/make.conf.anaconda-backup")
		emerge = subprocess.check_output(["emerge", "--info"])
		shutil.move("/etc/make.conf.anaconda-backup", "/etc/make.conf")
		m = re.search('USE="(.*?)"', emerge)
		use_enabled = m.group(1)
		use_enabled = use_enabled.split()
		
		usef = open("/usr/portage/profiles/use.desc")
		lines = usef.read().split("\n")

		use = {}
		use_list = [] # Keep them sorted
		for line in lines:
			comment = re.search("#", line)
			if comment:
				# Truncate the line where the comment starts
				line = line[:comment.start()]
			s = line.partition(" - ")
			if s[0] and s[2]:
				use[s[0]] = s[2]
				use_list.append(s[0])
				
		table = self.xml.get_widget("use_table")
		
		for flag in use_list:
			cols = table.get_property("n-columns")
			rows = table.get_property("n-rows")
			table.resize(rows+1, cols)
			
			cb = gtk.CheckButton(label=flag)
			if flag in use_enabled:
				cb.set_active(True)
				self.use_profile.append(flag) # ignore non-user-settable flags
			
			l = gtk.Label(use[flag])
			l.set_alignment(0,0)
			
			table.attach(cb, 0, 1, rows, rows+1, gtk.FILL)
			table.attach(l, 1, 2, rows, rows+1)
			
			self.buttons.append(cb)
		
		return self.align

