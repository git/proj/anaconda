#
# custom_kernel_gui.py: gui kernel configuration.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui
import re
import subprocess

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class KernelWindow(InstallWindow):
	def getNext(self):
		box = self.xml.get_widget("kernel_viewport")
		terminal = VirtualTerminal()
		box.add(terminal)
		
		out = subprocess.check_output(["emerge", "-p", "gentoo-sources"])
		kernel_line = out.split("\n")[4].split()
		for x in kernel_line:
			m = re.search("sys-kernel/gentoo-sources-.*", kernel_line)
			if m != None:
				kernel = m.group(0)
		
		version = kernel.partition("gentoo-sources-")[2]
		(version_kernel, version_r, version_rn) = version.partition("-r")
		version_r = version_r+version_rn
		
		# Ensure the same version is emerged.
		terminal.run_command("emerge =gentoo-sources-"+version)
		terminal.run_command("cd /usr/src/linux-"+version_kernel+"-gentoo"+version_r)
		terminal.run_command("make nconfig")
		self.anaconda.kernel = version_kernel+"-gentoo"+version_r
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf
		
		# Skip if we're doing a genkernel
		if self.anaconda.genkernel:
			return None
		
		(self.xml, self.align) = gui.getGladeWidget("custom_kernel.glade", "custom_kernel_align")
		
		return self.align

