#
# mirrorselect-sync_gui.py: gui mirrorselect.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui
import os
from mirrorselect_gui import MirrorselectWindow

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class MirrorselectSyncWindow(InstallWindow):
	def getNext(self):
		for button in self.buttons:
			if button.get_property("active"):
				# For while the treeview is broken
				#self.anaconda.mirrors_sync.append(button.get_property("label"))
				self.anaconda.mirrors_sync.append("rsync://rsync.gentoo.org/gentoo-portage")
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf  
		ms = MirrorselectWindow(InstallWindow)
		
		(self.xml, self.align) = gui.getGladeWidget("mirrorselect.glade", "mirrorselect_align")
		
		self.intro_label = self.xml.get_widget("intro_label")
		self.intro_label.set_label(_("Choose your Portage sync mirror(s).\nThese are used to sync your Portage tree."))
		
		mirrorsf = None
		
		while mirrorsf == None:
			# Try downloading the mirrorlist
			mirrorsf = ms.downloadMirrorlist()
			# if failed, 
			if mirrorsf == None:
				md = gtk.MessageDialog(buttons=gtk.BUTTONS_YES_NO)
				md.set_property("title", _("Failed to download mirrorlist"))
				md.set_property("text", _("Failed to download the mirror list. Would you like to try again?\nPressing 'No' will abort the installation."))
				if md.run() == gtk.RESPONSE_NO:
					md.destroy()
					InstallControlWindow._doExit([])
					
		mirrors_parsed = ms.parseMirrors(mirrorsf)
		self.mirrors = ms.mirrorList(mirrors_parsed, "rsync")
	
		ms.addMirrors(self.mirrors, mirrors_parsed, self.xml)
		self.buttons = ms.buttons
	
		return self.align
