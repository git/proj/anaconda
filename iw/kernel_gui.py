#
# kernel_gui.py: gui kernel selector (genkernel/custom)s.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class KernelWindow(InstallWindow):
	def getNext(self):
		if self.xml.get_widget("genkernel").get_property("active"):
			self.anaconda.genkernel = True
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf
		(self.xml, self.align) = gui.getGladeWidget("kernel.glade", "kernel_align")
		
		return self.align

