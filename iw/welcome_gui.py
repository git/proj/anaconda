#
# welcome_gui.py: gui welcome screen.
#
# Copyright (C) 2000, 2001, 2002  Red Hat, Inc.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import gtk
import gui
import sys
from iw_gui import *

from constants import *
from gui import InstallControlWindow
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class WelcomeWindow (InstallWindow):
    
    # We have to do it this way with the "\n"s because GTK+ is retarded.
    # https://bugzilla.gnome.org/show_bug.cgi?id=318276
    windows = { 
        "welcomescreen" : (
            _("Welcome to the Gentoo installer!"),
            _("\
This wizard will take you through installing Gentoo on your computer. We will\n\
follow the official Gentoo Handbook (with a few changes) and if at any point\n\
you'd like to consult the handbook, you'll find the link in the top-right corner\n\
 of the window.\n\n\
We'll start off asking you for the language and keyboard layout you'd like the\n\
installer and your future system to use. We will then skip straight to Chapter 4\n\
and continue with the installation."),
            0, "Gentoo Handbook"),
                       
        "preparedisks" : (
            _("Preparing the Disks"),
            _("\
To be able to install Gentoo, you must create the necessary partitions. The \n\
following screens will help you choose the right disk configuration for your \n\
system."),
            4, None),
                          
        "installationfiles" : (
            _("Installing the Gentoo Installation Files"),
            _("\
Gentoo installs work through a stage3 archive. In this chapter the installer \n\
will help you choose the right compilation options for your machine. "),
            5, None),
                          
        "basesystem" : (
            _("Installing the Gentoo Installation Files"),
            _("\
In this chapter you will set your Gentoo mirrors, select your profile and choose\n\
 your the USE flags."),
            6, None),
                       
        "configurekernel": (
            _("Configuring the Kernel"),
            _("\
The Linux kernel is the core of every distribution. This chapter explains how to\n\
configure your kernel. You will only have to set your timezone, as the \n\
installer will just copy the LiveDVD kernel to your new system."),
            7, None),
                        
        "configuresystem" : (
            _("Configuring your System"),
            _("\
In this chapter you will only have to set your root password as the rest is\n\
figured out automatically ;-)"),
            8, None),
                       
        "systoolswelcome" : (
            _("Installing Necessary System Tools"),
            _("\
In this chapter we help you choose and install some important tools."),
            9, None),
                       
        "bootloaderwelcome" : (
            _("Configuring the Bootloader"),
            _("\
In this chapter you'll be able configure the GRUB bootloader."),
            10, None),
                       
        "finalizing" : (
            _("Finalizing your Gentoo Installation"),
            _("\
You're almost done. We'll just create a user for your system.\n\n\
You'll also be able to choose if you'd like the X Window System and a desktop\n\
environment installed on your new system. "),
            11, None)
    }
    
    def getScreen(self, anaconda):
        (step, _anaconda) = anaconda.dispatch.currentStep()
        (title, text, chapter, linktext) = self.windows[step]
        return self.setWindow(anaconda, title, text, chapter, linktext)
    
    def setWindow(self, anaconda, title, text, chapter, linktext):
        (xml, align) = gui.getGladeWidget("welcome.glade", "welcome_align")
        title_ = xml.get_widget("title")
        text_ = xml.get_widget("text")
        title_.set_markup("<span size=\"x-large\">%s</span>" % (_("Chapter %i: %s") % (chapter, title)))
        text_.set_text(text)
        anaconda._intf.icw.set_chapter(chapter, title, "http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=%i"%chapter)
        return align
    
    
    def getNext(self):
       return None
