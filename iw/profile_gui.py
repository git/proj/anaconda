#
# profile_gui.py: gui profile selection.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui
import subprocess

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class ProfileWindow(InstallWindow):
	def getNext(self):
		for button in self.buttons:
			if button.get_property("active"):
				self.anaconda.profile = button.get_property("label")
		# We need this for the USE flags
		subprocess.call(["eselect", "profile", "set", self.anaconda.profile])
		if self.anaconda.profile == None:
			self.anaconda.intf.messageWindow(_("Select profile"), _("You must select a profile!"))
			raise gui.StayOnScreen
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf        

		(self.xml, self.align) = gui.getGladeWidget("profile.glade", "profile_align")
		
		out = subprocess.check_output(["eselect", "profile", "list"])
		lines = out.split("\n")
		del lines[0]
		
		profiles = []
		for line in lines:
			s = line.split()
			if s != []:
				profiles.append(s[1])
				
		box = self.xml.get_widget("profiles_box")
		
		# This isn't on the page but is required to make a group dynamically.
		dummy = gtk.RadioButton()
		
		for profile in profiles:
			cb = gtk.RadioButton(group=dummy, label=profile, use_underline=False)
			box.pack_start(cb)
		
		self.buttons = dummy.get_group()
		
		return self.align

