#
# makeconf_gui.py: gui make.conf settings.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui
import os

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class MakeconfWindow(InstallWindow):
	
	def err(title, message):
		self.anaconda.intf.messageWindow(title, message, custom_icon="error")
		raise gui.StayOnScreen
	
	def getNext(self):
		if self.march.get_active_text() == "":
			self.err(_("Select -march"), _("You need to choose the -march setting!"))
		if self.opt.get_active_text() == "":
			self.err(_("Select -O"), _("You need to choose the -O setting!"))
		
		self.anaconda.makeconf_march =  self.march.get_active_text()
		self.anaconda.makeconf_opt =    self.opt.get_active_text()
		self.anaconda.makeconf_pipe =   self.pipe.get_property("active")
		self.anaconda.makeconf_fomit =  self.fomit.get_property("active")
		self.anaconda.makeconf_jobs =   self.jobs.get_value_as_int()
		self.anaconda.makeconf_load =   self.load.get_value_as_int()
		self.anaconda.makeconf_silent = self.silent.get_property("active")
		return None

	def getScreen(self, anaconda):
		if os.environ["ANACONDA_PRODUCTARCH"] == "x86":
			marches = [
				"prescott",
				"pentium-m",
				"athlon-4",
				"athlon-xp",
				"athlon-mp",
				"athlon",
				"athlon-tbird",
				"pentium4",
				"pentium3",
				"pentium2",
				"k6-2",
				"k6-3",
				"k6",
				"i686",
				"pentiumpro",
			]
		if os.environ["ANACONDA_PRODUCTARCH"] == "amd64":
			marches = ["nocona", "k8", "opteron", "athlon64", "athlon-fx"]
			
		self.anaconda = anaconda
		self.intf = anaconda.intf        

		(self.xml, self.align) = gui.getGladeWidget("makeconf.glade", "makeconf_align")

		self.march =  self.xml.get_widget("march")
		self.opt =    self.xml.get_widget("opt")
		self.pipe =   self.xml.get_widget("pipe")
		self.fomit =  self.xml.get_widget("fomit")
		self.jobs =   self.xml.get_widget("jobs")
		self.load =   self.xml.get_widget("load")
		self.silent = self.xml.get_widget("silent")
		
		for x in marches:
			self.march.append_text(x) 

		# TODO: Get a list of arches available for this installation.

		return self.align

