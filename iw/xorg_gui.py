#
# xorg_gui.py: gui Xorg/DE/WM settings.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class XorgWindow(InstallWindow):
	des = ["fluxbox", "gnome", "kde", "lxde", "xfce4"]
	
	def getNext(self):
		for de in self.des:
			cb = self.xml.get_widget(de)
			if cb.get_property("active"):
				self.anaconda.xorg = de
		# All DEs need the X use flag
		if self.anaconda.xorg:
			if "X" not in self.anaconda.use_flags:
				self.anaconda.use_flags.append("X")
			if "-X" in self.anaconda.use_flags:
				f = self.anaconda.use_flags.index("-X")
				del self.anaconda.use_flags[f]
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf        

		(self.xml, self.align) = gui.getGladeWidget("xorg.glade", "xorg_align")

		for de in self.des:
			icon = self.xml.get_widget(de+"_icon")
			gui.readImageFromFile(de+".png", image=icon)

		return self.align

