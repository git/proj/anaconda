#
# systools_gui.py: gui cron and system logger selection.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pango
import sys
import gui

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class SystoolsWindow(InstallWindow):
	def getNext(self):
		self.anaconda.cron = self.cron.get_active_text()
		self.anaconda.syslog = self.syslog.get_active_text()
		if self.anaconda.syslog == "":
			self.anaconda.intf.messageWindow(_("Select syslog"), _("You must select a system logger!"))
			raise gui.StayOnScreen
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf        

		(self.xml, self.align) = gui.getGladeWidget("systools.glade", "systools_align")
		
		self.cron = self.xml.get_widget("cron")
		self.syslog = self.xml.get_widget("logger")
		
		return self.align

