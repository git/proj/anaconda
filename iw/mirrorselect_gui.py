#
# mirrorselect_gui.py: gui mirrorselect.
#
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string
import gtk
import gtk.glade
import gtk.gdk
import gobject
import pygtk
import pango
import sys
import gui
import os
from mirrorselect.mirrorparser3 import MirrorParser3

from iw_gui import *

from constants import *
import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

class MirrorselectWindow(InstallWindow):
	buttons = []
	
	def getNext(self):
		for button in self.buttons:
			if button.get_property("active"):
				# For while the treeview is broken
				#self.anaconda.mirrors.append(button.get_property("label"))
				self.anaconda.mirrors.append("http://mirrors.kernel.org/gentoo/")
		return None

	def getScreen(self, anaconda):
		self.anaconda = anaconda
		self.intf = anaconda.intf        
		
		(self.xml, self.align) = gui.getGladeWidget("mirrorselect.glade", "mirrorselect_align")
		
		mirrorsf = None
		
		while mirrorsf == None:
			# Try downloading the mirrorlist
			mirrorsf = self.downloadMirrorlist()
			# if failed, 
			if mirrorsf == None:
				md = gtk.MessageDialog(buttons=gtk.BUTTONS_YES_NO)
				md.set_property("title", _("Failed to download mirrorlist"))
				md.set_property("text", _("Failed to download the mirror list. Would you like to try again?\nPressing 'No' will abort the installation."))
				if md.run() == gtk.RESPONSE_NO:
					md.destroy()
					InstallControlWindow._doExit([])
					
		mirrors_parsed = self.parseMirrors(mirrorsf)
		self.mirrors = self.mirrorList(mirrors_parsed, "http+ftp")
		
		self.addMirrors(self.mirrors, mirrors_parsed, self.xml)
	
		return self.align
	
	def addMirrors(self, mirror_list, mirror_data, xml):
		(regions, countries, mirrors) = mirror_list
		self.treestore = gtk.TreeStore(bool, str, str, str)
		for region in regions:
			region_ts = self.treestore.append(None, [False, region, "", ""])
			for country in countries[region]:
				country_ts = self.treestore.append(region_ts, [False, country, "", ""])
				for mirror in mirrors[country]:
					self.addMirrorRow(self.treestore, country_ts, region, country, mirror, mirror_data[region][country][mirror])
		treeview = gtk.TreeView(self.treestore)
		url_column = gtk.TreeViewColumn(_(""))
		treeview.append_column(url_column)
		name_column = gtk.TreeViewColumn(_("Mirror Name"))
		treeview.append_column(name_column)
		ipv4_column = gtk.TreeViewColumn("IPv4?")
		treeview.append_column(ipv4_column)
		ipv6_column = gtk.TreeViewColumn("IPv6?")
		treeview.append_column(ipv6_column)
		url_cell = gtk.CellRendererToggle()
		text_cell = gtk.CellRendererText()
		url_column.pack_start(url_cell, True)
		name_column.pack_start(text_cell, True)
		ipv4_column.pack_start(text_cell, True)
		ipv6_column.pack_start(text_cell, True)
		url_column.add_attribute(url_cell, "active", 0)
		name_column.add_attribute(text_cell, "text", 1)
		ipv4_column.add_attribute(text_cell, "text", 2)
		ipv6_column.add_attribute(text_cell, "text", 3)
		treeview.set_search_column(1)
		url_cell.connect("toggled", self.toggleCB, self.treestore)
		
		xml.get_widget("mirrors_viewport").add(treeview)
		
	def toggleCB(self, widget, path, model):
		model[path][0] = not model[path][0]
	
	def addMirrorRow(self, ts, country_ts, region, country, mirror, data):
		ipv4 = ""
		ipv6 = ""
		if data["ipv4"] == "y":
			ipv4 = "ipv4"
		if data["ipv6"] == "y":
			ipv6 = " ipv6"
		
		ts.append(country_ts, [False, mirror, ipv4, ipv6])
	
	def downloadMirrorlist(self):
		try:
			os.system("wget -nc -O /tmp/mirror3.xml %s" % os.environ["PORTAGE_MIRRORLIST_URL"])
			f = open("/tmp/mirror3.xml")
		except:
			return None
		return f
	
	def parseMirrors(self, mirrorlist):
		xml = mirrorlist.read()
		parser = MirrorParser3()
		parser.parse(xml)
		tuples = parser.tuples()
			
		mirrors = {}
		for mirror in tuples:
			if mirror[1]["region"] not in mirrors:
				mirrors[mirror[1]["region"]] = {}
			if mirror[1]["country"] not in mirrors[mirror[1]["region"]]:
				mirrors[mirror[1]["region"]][mirror[1]["country"]] = {}
			mirrors[mirror[1]["region"]][mirror[1]["country"]]["%s (%s)" % (mirror[1]["name"], mirror[1]["proto"])] = \
				{"url": mirror[0],
				 "name": mirror[1]["name"],
				 "proto": mirror[1]["proto"],
				 "ipv4": mirror[1]["ipv4"],
				 "ipv6": mirror[1]["ipv6"]}
			
		return mirrors
	
	def mirrorList(self, mirrors_parsed, proto):
		region_list = mirrors_parsed.keys()
		region_list.sort()
		countries = {}
		mirrors = {}
		mirrors_proto = {}
		for region in region_list:
			countries[region] = mirrors_parsed[region].keys()
			countries[region].sort()
			for country in countries[region]:
				mirrors[country] = mirrors_parsed[region][country].keys()
				mirrors[country].sort()
				mirrors_proto[country] = []
				for mirror in mirrors[country]:
					if proto == "http+ftp":
						if mirrors_parsed[region][country][mirror]["proto"] == "http":
							mirrors_proto[country].append(mirror)
						if mirrors_parsed[region][country][mirror]["proto"] == "ftp":
							mirrors_proto[country].append(mirror)
					if mirrors_parsed[region][country][mirror]["proto"] == proto:
						mirrors_proto[country].append(mirror)
				mirrors_proto[country].sort()
				
		return (region_list, countries, mirrors_proto)
	
