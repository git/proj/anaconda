#!/bin/sh
GENTOO_VER=${GENTOO_VER:-5.3}
rm -rf tmp
mkdir tmp
sh autogen.sh && \
	./configure --prefix=/usr --disable-selinux --enable-selinux=no && \
	make && make DESTDIR="${PWD}"/tmp install && \
	( find "${PWD}"/tmp -name "*.py[co]" | xargs rm ) && \
	( find "${PWD}"/tmp -name "*.*a" | xargs rm ) && \
	( find "${PWD}"/tmp -name "*.so" | xargs rm )
[[ "${?}" != "0" ]] && exit 1

pyanaconda_dir=$(find tmp -name "pyanaconda" -type d)
[[ -d "${pyanaconda_dir}" ]] || { echo "ouch" && exit 1 }
cd $(dirname "${pyanaconda_dir}")
tar cjvf pyanaconda-${GENTOO_VER}.tar.bz2 "$(basename ${pyanaconda_dir})"
md5sum pyanaconda-${GENTOO_VER}.tar.bz2 > pyanaconda-${GENTOO_VER}.tar.bz2.md5
mv pyanaconda-${GENTOO_VER}.tar.bz2{,.md5} "${OLDPWD}"/
cd "${OLDPWD}"
make distclean
rm -rf tmp
echo
echo "Done cooking pyanaconda-${GENTOO_VER}.tar.bz2"
