#
# gentoo/__init__.py: Gentoo Portage Anaconda install method backend
#
#
# Copyright (C) 2011 wiktor w brodlo <wiktor@brodlo.net>
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys

class Portage:

	def __init__(self, terminal, root):
		print "Portage init!"
		self.term = terminal
		self.root = root

	# Syncs the Portage tree and updates Portage if an update is available
	def sync(self):
		self.term.run_command("chroot "+self.root+" emerge --sync")
		if not os.path.exists(self.root+"/usr/portage/metadata/timestamp"):
			self.term.run_command("chroot "+self.root+" emerge-webrsync")
			if self.term.get_child_exit_status() != 0:
				return False
		self.term.run_command("chroot "+self.root+" emerge --update portage")
		if self.term.get_child_exit_status() == 0:
			return True
		return False

	# Installs a package atom
	def install(self, atom):
		self.term.run_command("chroot "+self.root+" emerge "+atom)
		if self.term.get_child_exit_status() == 0:
			return True
		return False

	# Updates world
	def update_world(self):
		self.term.run_command("chroot "+self.root+" emerge --deep --newuse --update world")
		if self.term.get_child_exit_status() == 0:
			return True
		return False

	# Removes a package atom
	def remove(self, atom):
		self.term.run_command("chroot "+self.root+" emerge -C "+atom)
		if self.term.get_child_exit_status() == 0:
			return True
		return False

