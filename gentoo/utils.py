# -*- coding: utf-8 -*-
#
# gentoo/utils.py
#
# Copyright (C) 2010 Fabio Erculiani
# Copyright (C) 2011 wiktor w brodlo
# Copyright (C) 2011 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Glib imports
import glib

# Python imports
import os
import sys
import stat
import sys
import subprocess
import commands
import shutil
import statvfs
import tempfile
import time

# Anaconda imports
import logging
from constants import productPath
from gentoo import Portage
from gentoo.const import LIVE_USER, LANGUAGE_PACKS, REPO_NAME

import gettext
_ = lambda x: gettext.ldgettext("anaconda", x)

STDERR_LOG = open("/tmp/anaconda.log","aw")
log = logging.getLogger("anaconda")

class GentooProgress:

    def __init__(self, anaconda):
        self._intf = anaconda.intf
        self._display_mode = anaconda.displayMode
        self._prog = self._intf.instProgress
        self.__updater = None
        self._pix_count = 0
        self.__alive = False
        self.__adbox_running = False
        self.__image_t = time.time()

    def _process_events(self):
        self._prog.processEvents()
        return self.__alive

    def start(self):
        self.__alive = True
        if (self.__updater is None) and (self._display_mode == "g"):
            self.__updater = glib.timeout_add(2000, self._process_events,
                priority = glib.PRIORITY_HIGH + 10)
        if self._display_mode == "g":
            self.__adbox_running = True

    def __kill_updater(self):
        if self.__updater is not None:
            glib.source_remove(self.__updater)
            self.__updater = None

    def stop(self):
        self.__kill_updater()
        self.__alive = False

    def progress(self):
        return self._prog

    def set_label(self, label):
        if self._display_mode == "g":
            def do_it():
                self._prog.set_label(label)
                return False
            glib.idle_add(do_it)
            self._process_events()
        else:
            self._prog.set_label(label)

    def set_text(self, text):
        if self._display_mode == "g":
            def do_it():
                self._prog.set_text(text)
                return False
            glib.idle_add(do_it)
            self._process_events()
        else:
            self._prog.set_text(text)

    def set_fraction(self, pct):

        if pct > 1.0:
            pct = 1.0
        elif pct < 0.0:
            pct = 0.0

        if self._display_mode == "g":
            def do_it(pct):
                self._prog.set_fraction(pct)
                return False
            glib.idle_add(do_it, pct)
            self._process_events()
        else:
            self._prog.set_fraction(pct)
 
    # TODO: Check if we need this
    #def _spawn_adimage(self):

    #    if not self.__adbox_running:
    #        return

    #    cur_t = time.time()
    #    if cur_t <= (self.__image_t + 10):
    #        return
    #    self.__image_t = cur_t

    #    pixmaps = getattr(self._prog, 'pixmaps', [])
    #    pix_len = len(pixmaps)
    #    if pix_len == 0:
    #        log.warning("Shutting down _spawn_adimage, no images")
    #        self.__adbox_running = False
    #        return

    #    if not self._prog.adpix:
    #        log.warning("Shutting down _spawn_adimage, no adpix")
    #        self.__adbox_running = False
    #        return

    #    try:
    #        pix_path = pixmaps[self._pix_count]
    #    except IndexError:
    #        self._pix_count = 0
    #        pix_path = pixmaps[0]

    #    self._pix_count += 1

    #    import gui
    #    pix = gui.readImageFromFile(pix_path)
    #    if pix:
    #        self._prog.adbox.remove(self._prog.adpix)
    #        self._prog.adpix.destroy()
    #        pix.set_alignment(0.5, 0.5)
    #        self._prog.adbox.add(pix)
    #        self._prog.adpix = pix
    #    else:
    #        log.warning("Shutting down _spawn_adimage, no pixmap: %s" % (
    #            pix_path,))

    #    self._prog.adbox.show_all()


class GentooInstall:

    def __init__(self, anaconda):

        self._anaconda = anaconda
        self._root = anaconda.rootPath
        self._prod_root = productPath
        self._intf = anaconda.intf
        self._progress = GentooProgress(anaconda)
        self._portage = Portage(anaconda._intf.instProgress.terminal, self._root)
		# I'm wondering what's it actually for. ~w.
        #self._settings = SystemSettings()
        with open("/proc/cmdline", "r") as cmd_f:
            self.cmdline = cmd_f.readline().strip().split()
        #sys.stderr = STDERR_LOG

        # TODO: Remove or change this
        #self._files_db_path = self._root+"/files.db"
        #try:
        #    self._files_db = self._entropy.open_generic_repository(
        #         self._files_db_path, dbname = "filesdb",
        #        indexing_override = True)
        #except TypeError:
        #    # new API
        #    self._files_db = self._entropy.open_generic_repository(
        #         self._files_db_path, name = "filesdb",
        #        indexing_override = True)
        #if hasattr(self._files_db, "initializeDatabase"):
        #    self._files_db.initializeDatabase()
        #else:
        #    self._files_db.initializeRepository()
        #self._live_repo = self._open_live_installed_repository()
        #self._package_identifiers_to_remove = set()

    def destroy(self):
        # remove files db if exists
        #if hasattr(self._files_db, "close"):
        #    self._files_db.close()
        #else:
        #    self._files_db.closeDB()
        #try:
        #    os.remove(self._files_db_path)
        #except OSError:
        #    pass

        self._progress.stop()

    def spawn_chroot(self, args, silent = False):

        pid = os.fork()
        if pid == 0:

            os.chroot(self._root)
            os.chdir("/")
            do_shell = False
            myargs = args
            if not isinstance(args, (list, tuple)):
                do_shell = True
            if silent:
                p = subprocess.Popen(args, stdout = STDERR_LOG,
                    stderr = STDERR_LOG, shell = do_shell)
            else:
                p = subprocess.Popen(args, shell = do_shell)
            rc = p.wait()
            os._exit(rc)

        else:

            rcpid, rc = os.waitpid(pid,0)
            return rc

    def spawn(self, args):
        myargs = args
        if isinstance(args, (list, tuple)):
            myargs = ' '.join(args)
        return subprocess.call(myargs, shell = True)

    #def _open_live_installed_repository(self):
    #    dbpath = self._prod_root + etpConst['etpdatabaseclientfilepath']
    #    try:
    #        dbconn = self._entropy.open_generic_repository(
    #            dbpath, xcache = False, read_only = True,
    #            dbname = "live_client", indexing_override = False)
    #    except TypeError:
    #        # new API
    #        dbconn = self._entropy.open_generic_repository(
    #            dbpath, xcache = False, read_only = True,
    #            name = "live_client", indexing_override = False)
    #    return dbconn

    #def _change_entropy_chroot(self, chroot = None):
    #    if not chroot:
    #        self._entropy._installed_repo_enable = True
    #        self._entropy.noclientdb = False
    #    else:
    #        self._entropy._installed_repo_enable = False
    #        self._entropy.noclientdb = True
    #    if chroot is None:
    #        chroot = ""
    #    self._entropy.switch_chroot(chroot)
    #    sys_settings_plg_id = etpConst['system_settings_plugins_ids']['client_plugin']
    #    del self._settings[sys_settings_plg_id]['misc']['configprotectskip'][:]

    def install_package(self, atom, match = None, silent = False, fetch = False):
        if not self._portage.install(atom):
            log = open("/tmp/emerge-fail.log", "a")
            log.write(atom+" failed to install, trying again with make -j1\n")
            log.close()
            os.environ["MAKEOPTS"] = "-j1"
            if not self._portage.install(atom):
                log = open("/tmp/emerge-fail.log", "a")
                log.write(atom+" failed to install the second time :(\n")
                log.close()
            del os.environ["MAKEOPTS"]

    # TODO: We don't remove anything from stage3
    def remove_package(self, atom, match = None, silent = False):

    #    if silent and os.getenv('GENTOO_DEBUG'):
    #        silent = False

    #    chroot = self._root
    #    root = etpSys['rootdir']
    #    if chroot != root:
    #        self._change_entropy_chroot(chroot)

    #    if match is None:
    #        match = self._entropy.installed_repository().atomMatch(atom)

    #    oldstdout = sys.stdout
    #    if silent:
    #        sys.stdout = STDERR_LOG
    #        etpUi['mute'] = True

    #    try:
    #        rc = 0
    #        if match[0] != -1:
    #            Package = self._entropy.Package()
    #            Package.prepare((match[0],), "remove")
    #            if 'remove_installed_vanished' not in Package.pkgmeta:
    #                rc = Package.run()
    #                Package.kill()
    #    finally:
    #        if silent:
    #            sys.stdout = oldstdout
    #            etpUi['mute'] = False

    #    if chroot != root:
    #        self._change_entropy_chroot(root)

        return self._portage.remove(atom)

    #def install_package_file(self, package_file):
    #    chroot = self._root
    #    root = etpSys['rootdir']
    #    if chroot != root:
    #        self._change_entropy_chroot(chroot)

    #    try:
    #        atomsfound = self._entropy.add_package_repository(
    #            package_file)
    #    except EntropyPackageException:
    #        return -1
    #    repo = 0
    #    for match in atomsfound:
    #        repo = match[1]
    #        Package = self._entropy.Package()
    #        Package.prepare(match, "install")
    #        rc2 = Package.run()
    #        if rc2 != 0:
    #            if chroot != root:
    #                self._change_entropy_chroot(root)
    #            return rc2
    #        Package.kill()

    #    if chroot != root:
    #        self._change_entropy_chroot(root)

    #    if repo != 0:
    #        self._entropy.remove_repository(repo)

    #    return 0

    def _configure_skel(self):
        pass

        # copy Sulfur on the desktop
    #    sulfur_desktop = self._root+"/usr/share/applications/sulfur.desktop"
    #    if os.path.isfile(sulfur_desktop):
    #        sulfur_user_desktop = self._root+"/etc/skel/Desktop/sulfur.desktop"
    #        shutil.copy2(sulfur_desktop, sulfur_user_desktop)
    #        try:
    #            os.chmod(sulfur_user_desktop, 0775)
    #        except OSError:
    #            pass

    #    gparted_desktop = self._root+"/etc/skel/Desktop/gparted.desktop"
    #    if os.path.isfile(gparted_desktop):
    #        os.remove(gparted_desktop)

    #    installer_desk = self._root+"/etc/skel/Desktop/liveinst.desktop"
    #    if os.path.isfile(installer_desk):
    #        os.remove(installer_desk)

    #    # install welcome loader
    #    orig_welcome_desk = self._root+"/etc/gentoo/gentoo-welcome-loader.desktop"
    #    if os.path.isfile(orig_welcome_desk):
    #        autostart_dir = self._root+"/etc/skel/.config/autostart"
    #        if not os.path.isdir(autostart_dir):
    #            os.makedirs(autostart_dir)
    #        desk_name = os.path.basename(orig_welcome_desk)
    #        desk_path = os.path.join(autostart_dir, desk_name)
    #        shutil.copy2(orig_welcome_desk, desk_path)

    def _is_virtualbox(self):
        if not os.path.isfile("/etc/init.d/virtualbox-guest-additions"):
            return False
        sts = os.system("/usr/sbin/lspci -n | grep \" 80ee:\" &> /dev/null")
        if sts == 0:
            return True
        return False

    def _is_encrypted(self):
        if self._anaconda.storage.encryptionPassphrase:
            return True
        return False

    def configure_services(self):

        action = _("Configuring System Services")
        self._progress.set_text(action)

        # Installer services
        config_script = """
            rc-update add vixie-cron default
            if [ ! -e "/etc/init.d/net.eth0" ]; then
                cd /etc/init.d && ln -s net.lo net.eth0
            fi
            if [ -e "/etc/init.d/nfsmount" ]; then
                rc-update add nfsmount default
            fi
            if [ -e "/etc/init.d/cdeject" ]; then
                rc-update del cdeject shutdown
            fi
            if [ -e "/etc/init.d/oemsystem-boot" ]; then
                rc-update add oemsystem-boot boot
            fi
            if [ -e "/etc/init.d/oemsystem-default" ]; then
                rc-update add oemsystem-default default
            fi
        """
        self.spawn_chroot(config_script, silent = True)


        # setup dmcrypt service if user enabled encryption
        if self._is_encrypted():
            self.spawn_chroot("rc-update add dmcrypt boot", silent = True)

        if self._is_virtualbox():
            self.spawn_chroot("rc-update add virtualbox-guest-additions boot",
                silent = True)
        else:
            self.spawn_chroot("rc-update del virtualbox-guest-additions boot",
                silent = True)

        # Copy the kernel modules blacklist configuration file
        if os.access("/etc/modules.d/blacklist",os.F_OK):
            self.spawn(
                "cp -p /etc/modules.d/blacklist %s/etc/modules.d/blacklist" % (
                    self._root,))

    # TODO: Check this after we have X working
    def remove_proprietary_drivers(self):
        """
        Detect a possible OSS video card and remove /etc/env.d/*ati
        """
        if self._get_opengl() == "xorg-x11":
            ogl_script = """
                rm -f /etc/env.d/09ati
                rm -rf /usr/lib/opengl/ati
                rm -rf /usr/lib/opengl/nvidia
            """
            self.spawn_chroot(ogl_script)
            self.remove_package('ati-drivers', silent = True)
            self.remove_package('nvidia-settings', silent = True)
            self.remove_package('nvidia-drivers', silent = True)

    def copy_udev(self):
        tmp_dir = tempfile.mkdtemp()
        self.spawn("mount --move %s/dev %s" % (self._root, tmp_dir,))
        self.spawn("cp /dev/* %s/dev/ -Rp" % (self._root,))
        self.spawn("cp /dev/.u* %s/dev/ -Rp" % (self._root,))
        self.spawn("mount --move %s %s/dev" % (tmp_dir, self._root,))
        shutil.rmtree(tmp_dir, True)

    def _get_opengl(self, chroot = None):
        """
        get the current OpenGL subsystem (ati,nvidia,xorg-x11)
        """

        if chroot is None:
            oglprof = os.getenv('OPENGL_PROFILE')
            if oglprof:
                return oglprof
            chroot = ""

        ogl_path = chroot+"/etc/env.d/03opengl"
        if os.path.isfile(ogl_path) and os.access(ogl_path,os.R_OK):
            f = open(ogl_path,"r")
            cont = [x.strip() for x in f.readlines() if \
                x.strip().startswith("OPENGL_PROFILE")]
            f.close()
            if cont:
                xprofile = cont[-1]
                if "nvidia" in xprofile:
                    return "nvidia"
                elif "ati" in xprofile:
                    return "ati"

        return "xorg-x11"

    def setup_users(self):

        # configure .desktop files on Desktop
        self._configure_skel()

        # remove live user and its home dir
        pid = os.fork()
        if pid > 0:
            os.waitpid(pid, 0)
        else:
            os.chroot(self._root)
            # backward compat
            proc = subprocess.Popen(("userdel", "-f", "-r", LIVE_USER),
                stdout = subprocess.PIPE, stderr = subprocess.PIPE)
            os._exit(proc.wait())

        # fixup root password
        # see bug #2175
        pid = os.fork()
        if pid > 0:
            os.waitpid(pid, 0)
        else:
            os.chroot(self._root)
            root_pass = self._anaconda.users.rootPassword["password"]
            root_str = "root:%s\n" % (root_pass,)
            proc = subprocess.Popen(["chpasswd"],
                stdin = subprocess.PIPE,
                stdout = subprocess.PIPE, stderr = subprocess.PIPE)
            proc.stdin.write(root_str)
            proc.stdin.close()
            os._exit(proc.wait())

    def setup_manual_networking(self):
        mn_script = """
            rc-update del NetworkManager default
            rc-update del avahi-daemon default
            rc-update del dhcdbd default
            if [ -f "/etc/rc.conf" ]; then
                sed -i 's/^#rc_hotplug=".*"/rc_hotplug="*"/g' /etc/rc.conf
                sed -i 's/^rc_hotplug=".*"/rc_hotplug="*"/g' /etc/rc.conf
            fi
        """
        self.spawn_chroot(mn_script, silent = True)

    def setup_networkmanager_networking(self):
        # our NM hook already mounts network shares
        nm_script = """
            rc-update del netmount default
            rc-update del nfsmount default
        """
        self.spawn_chroot(nm_script, silent = True)

    def get_keyboard_layout(self):
        console_kbd = self._anaconda.keyboard.get()
        kbd = self._anaconda.keyboard.modelDict[console_kbd]
        (name, layout, model, variant, options) = kbd
        # for X, KDE and GNOME
        keylayout = layout.split(",")[0].split("_")[0]
        return console_kbd, keylayout, variant, options

    def setup_keyboard(self):
        console_kbd, keylayout, variant, options = self.get_keyboard_layout()
        def _spawn(args):
            subprocess.call(args, shell=True)
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s gnome &> /dev/null" % (
            self._root, keylayout))
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s kde &> /dev/null" % (
            self._root, keylayout))
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s lxde &> /dev/null" % (
            self._root, keylayout))
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s e17 &> /dev/null" % (
            self._root, keylayout))
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s %s %s xorg &> /dev/null" % (
            self._root, keylayout, variant, options))
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s system &> /dev/null" % (
            self._root, console_kbd))
        _spawn("ROOT=%s /sbin/keyboard-setup-2 %s xfce &> /dev/null" % (
            self._root, console_kbd))

    def setup_sudo(self):
        sudoers_file = self._root + '/etc/sudoers'
        if os.path.isfile(sudoers_file):
            self.spawn("sed -i '/NOPASSWD/ s/^/#/' %s" % (sudoers_file,))
            with open(sudoers_file, "a") as sudo_f:
                sudo_f.write("\n#Added by Gentoo Installer\n%wheel  ALL=ALL\n")
                sudo_f.flush()

    def setup_audio(self):
        asound_state = "/etc/asound.state"
        asound_state2 = "/var/lib/alsa/asound.state"
        if os.path.isfile(asound_state) and os.access(asound_state, os.R_OK):
            asound_state_dest_dir = os.path.dirname(self._root+asound_state)
            asound_state_dest_dir2 = os.path.dirname(self._root+asound_state2)

            if not os.path.isdir(asound_state_dest_dir):
                os.makedirs(asound_state_dest_dir, 0755)

            if not os.path.isdir(asound_state_dest_dir2):
                os.makedirs(asound_state_dest_dir2, 0755)

            source_f = open(asound_state, "r")
            dest_f = open(self._root+asound_state, "w")
            dest2_f = open(self._root+asound_state2, "w")
            asound_data = source_f.readlines()
            dest_f.writelines(asound_data)
            dest2_f.writelines(asound_data)
            dest_f.flush()
            dest_f.close()
            dest2_f.flush()
            dest2_f.close()
            source_f.close()

    def setup_xorg(self):
        # These days things should work without xorg.conf
        # Copy current xorg.conf
        #live_xorg_conf = "/etc/X11/xorg.conf"
        #if not os.path.isfile(live_xorg_conf):
        #    return
        #xorg_conf = self._root + live_xorg_conf
        #shutil.copy2(live_xorg_conf, xorg_conf)
        #shutil.copy2(live_xorg_conf, xorg_conf+".original")
        if self._anaconda.xorg:
            self._progress.set_text(_("Installing Xorg"))
            self.install_package("xorg-server")
            self.install_package("xterm")
        self._progress.set_fraction(0.35)
        if self._anaconda.xorg == "fluxbox":
            self.install_package("x11-wm/fluxbox")
        if self._anaconda.xorg == "gnome":
            self.install_package("gnome-base/gnome")
        if self._anaconda.xorg == "kde":
            self.install_package("kde-base/kde-meta")
        if self._anaconda.xorg == "lxde":
            self.install_package("lxde-base/lxde-meta")
        if self._anaconda.xorg == "xfce4":
            self.install_package("xfce-base/xfce4-meta")
            
    def setup_language(self):
        # Prepare locale variables

        info = self._anaconda.instLanguage.info
        f = open(self._root + "/etc/env.d/02locale", "w")
        for key in info.keys():
            if info[key] is not None:
                f.write("%s=\"%s\"\n" % (key, info[key]))
        f.flush()
        f.close()

        # write locale.gen
        supported_file = "/usr/share/i18n/SUPPORTED"
        if os.path.isfile(supported_file):
            f = open(supported_file, "r")
            locale_supported = [x.strip() for x in f.readlines()]
            f.close()

            libc_locale = info['LANG'].split(".")[0].split("@")[0]
            valid_locales = []
            for locale in locale_supported:
                if locale.startswith(libc_locale):
                    valid_locales.append(locale)

            f = open(self._root + "/etc/locale.gen", "w")
            f.write("en_US.UTF-8 UTF-8\n")
            f.write("en_US ISO-8859-1\n")
            for locale in valid_locales:
                f.write("%s\n" % (locale,))
            f.flush()
            f.close()

        localization = self._anaconda.instLanguage.instLang.split(".")[0]
        # Configure KDE language
        self.spawn_chroot(("/sbin/language-setup", localization, "kde"),
            silent = True)
        self.spawn_chroot(("/sbin/language-setup", localization, "openoffice"),
            silent = True)
        self.spawn_chroot(("/sbin/language-setup", localization, "mozilla"),
            silent = True)

    def setup_nvidia_legacy(self):

        # Configure NVIDIA legacy drivers, if needed
        running_file = "/lib/nvidia/legacy/running"
        drivers_dir = "/install-data/drivers"
        if not os.path.isfile(running_file):
            return
        if not os.path.isdir(drivers_dir):
            return

        f = open(running_file)
        nv_ver = f.readline().strip()
        f.close()

        if nv_ver.find("17x.xx.xx") != -1:
            nv_ver = "17"
        elif nv_ver.find("9x.xx") != -1:
            nv_ver = "9"
        else:
            nv_ver = "7"

        legacy_unmask_map = {
            "7": "=x11-drivers/nvidia-drivers-7*",
            "9": "=x11-drivers/nvidia-drivers-9*",
            "17": "=x11-drivers/nvidia-drivers-17*"
        }

        self.remove_package('nvidia-drivers', silent = True)
        for pkg_file in os.listdir(drivers_dir):

            if not pkg_file.startswith("x11-drivers:nvidia-drivers-"+nv_ver):
                continue

            pkg_filepath = os.path.join(drivers_dir, pkg_file)
            if not os.path.isfile(pkg_filepath):
                continue
            dest_pkg_filepath = os.path.join(self._root + "/", pkg_file)
            shutil.copy2(pkg_filepath, dest_pkg_filepath)

            rc = self.install_package_file(dest_pkg_filepath)

            # mask all the nvidia-drivers, this avoids having people
            # updating their drivers resulting in a non working system
            mask_file = os.path.join(self._root+'/',
                "etc/portage/packages/package.mask")
            unmask_file = os.path.join(self._root+'/',
                "etc/portage/packages/package.unmask")
            if os.access(mask_file, os.W_OK) and os.path.isfile(mask_file):
                f = open(mask_file,"aw")
                f.write("\n# added by Gentoo Installer\nx11-drivers/nvidia-drivers\n")
                f.flush()
                f.close()
            if os.access(unmask_file, os.W_OK) and os.path.isfile(unmask_file):
                f = open(unmask_file,"aw")
                f.write("\n# added by Gentoo Installer\n%s\n" % (
                    legacy_unmask_map[nv_ver],))
                f.flush()
                f.close()

            if rc != 0:
                question_text = "%s: %s" % (
                    _("An issue occured while installing"),
                    pkg_file,)
                buttons = [_("Meh.")]
                self._intf.messageWindow(_("Drivers installation issue"),
                    question_text, custom_icon="question", type="custom",
                    custom_buttons = buttons)

            try:
                os.remove(dest_pkg_filepath)
            except OSError:
                pass

            break

        # force OpenGL reconfiguration
        ogl_script = """
            eselect opengl set xorg-x11 &> /dev/null
            eselect opengl set nvidia &> /dev/null
        """
        self.spawn_chroot(ogl_script)

    def env_update(self):
        self.spawn_chroot("env-update &> /dev/null")

    def emit_install_done(self):
        # user installed Gentoo, w00hooh!
        pass

    def live_install(self):
        """
        This function copies the stage3 into self._root
        """
        action = _("Downloading stage3...")
        
        if os.environ["ANACONDA_PRODUCTARCH"] == "x86":
            arch = "x86"
            cpu = "i686"
        if os.environ["ANACONDA_PRODUCTARCH"] == "amd64":
            arch = "amd64"
            cpu = "amd64"
        
        latest_stage = "http://distfiles.gentoo.org/releases/%s/autobuilds/latest-stage3-%s.txt" % (arch, cpu)
        
        os.system("wget -nc -O /tmp/latest-stage %s " % latest_stage)
        f = open("/tmp/latest-stage")
        
        lines = f.read().split("\n")
        stage = lines[2]
        
        stageurl = "http://distfiles.gentoo.org/releases/%s/autobuilds/%s" % (arch, stage)
        
        self._anaconda._intf.instProgress.terminal.run_command("wget -nc -O "+productPath+" %s " % stageurl)
        
        action = _("Unpacking stage3")
        self._anaconda._intf.instProgress.terminal.run_command("tar xvjpf "+productPath+" -C "+self._root)

        os.system("mount -t proc none %s/proc" % self._root)
        shutil.copy2("/etc/resolv.conf", self._root+"/etc/")

        self._progress.set_fraction(1)

        self._progress.set_text(_("Unpacking stage3 complete"))

    def language_packs_install(self):
         return
    
    # TODO: Obviously make it do the thing
    def setup_portage_mirrors(self):
        self._progress.set_label("%s: %s" % (
            _("Setting up mirrors"), _("not yet..."),))

    def copy_portage(self):
        self._progress.set_fraction(0.0)
        self._progress.set_text(_("Syncing the Portage tree (can take a long time)"))
        self._portage.sync()
        # We need some packages from anaconda-overlay but ideally they should be added to portage
        self._progress.set_fraction(0.1)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/app-admin", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.2)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/app-cdr", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.3)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/app-misc", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.4)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/dev-python", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.5)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/dev-util", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.6)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/sys-apps", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.7)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/sys-block", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.8)
        subprocess.call(["rm", "-rf", self._root+"/usr/portage/sys-boot/grub"])
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/sys-boot", self._root+"/usr/portage/"])
        self._progress.set_fraction(0.9)
        subprocess.call(["cp", "--recursive", "/anaconda-overlay/sys-libs", self._root+"/usr/portage/"])
        self._progress.set_fraction(1)
        
    def install_setup_tools(self):
        self._progress.set_fraction(0.0)
        self._progress.set_text(_("Installing configuration helpers"))
        os.environ["USE"] = "multislot policykit -gtk -X -gpm -truetype"
        os.mkdir(self._root+"/etc/portage")
        keywordsf = open(self._root+"/etc/portage/package.keywords", "w")
        keywordsf.write("dev-python/rhpl ~amd64 ~x86\nsys-boot/grub **\nsys-boot/os-prober ~amd64 ~x86")
        keywordsf.close()
        self._progress.set_fraction(0.3)
        self.install_package("-1 =grub-1.99")
        self._progress.set_fraction(0.6)
        self.install_package("-1 libuser")
        self._progress.set_fraction(0.7)
        self.install_package("-1 keyboard-configuration-helpers")
        self._progress.set_fraction(0.8)
        self.install_package("-1 language-configuration-helpers")
        self._progress.set_fraction(0.9)
        del os.environ["USE"]
    
    def set_profile(self):
        self._progress.set_fraction(0.0)
        self._progress.set_text(_("Setting profile %s" % self._anaconda.profile))
        self.spawn_chroot("eselect profile set "+self._anaconda.profile)
        self._progress.set_fraction(1.0)
        
    def write_makeconf(self):
        text = self.makeconf_factory(self._anaconda, os.environ["ANACONDA_PRODUCTARCH"])
        makeconf = open(self._root+"/etc/make.conf", "w")
        makeconf.write(text)
        makeconf.close()
        
    def makeconf_factory(self, settings, arch):
        cflags = "-march=%s -O%s" % (settings.makeconf_march, settings.makeconf_opt)
        if settings.makeconf_pipe:
            cflags += " -pipe"
        if settings.fomit:
            cflags += " -fomit-frame-pointer"
        cflags = "CFLAGS=\"%s\"" % cflags
        
        cxxflags = "CXXFLAGS=\"${CFLAGS}\""
        
        if arch == "amd64":
            arch = "x86_64"
        chost = "CHOST=\"%s-pc-linux-gnu\"" % arch
        
        if settings.makeconf_jobs == 0:
            makeopts = "-j"
        else:
            makeopts = "-j%i" % settings.makeconf_jobs
        if settings.makeconf_load > 0:
            makeopts += " -l%i" % settings.makeconf_jobs
        if settings.makeconf_silent:
            makeopts += " -s"
        makeopts = "MAKEOPTS=\"%s\"" % makeopts
        
        gentoo_mirrors = "GENTOO_MIRRORS=\"%s\"" % " ".join(settings.mirrors)
        sync = "SYNC=\"%s\"" % " ".join(settings.mirrors_sync)
        use = "USE=\"%s\"" % " ".join(settings.use_flags)
        
        return "\n".join([cflags, cxxflags, chost, makeopts, gentoo_mirrors, sync, use])
        
    def install_systools(self):
        self._progress.set_text(_("Installing system tools"))
        self.install_package(self._anaconda.cron)
        self._progress.set_fraction(0.8)
        self.install_package(self._anaconda.syslog)
        self._progress.set_fraction(0.9)
        
    def install_kernel(self):
        self._progress.set_text(_("Installing the kernel"))
        if self._anaconda.genkernel:
            subprocess.call(["mount", "/dev/sr0", "/mnt/cdrom"])
            shutil.copy2("/mnt/cdrom/boot/gentoo", self._root+"/boot/kernel-gentoo")
            shutil.copy2("/mnt/cdrom/boot/gentoo.igz", self._root+"/boot/initrd-gentoo")
            self._progress.set_fraction(0.9)
            subprocess.call(["cp", "-PR", "/lib/modules", self._root+"/lib"])
        else:
            shutil.copy2("/usr/src/linux-"+self._anaconda.kernel, self._root+"/usr/src/")
            self._progress.set_fraction(0.5)
            self._progress.set_text(_("Compiling your custom kernel"))
            self._anaconda._intf.instProgress.terminal.run_command("chroot "+self._root+" eselect kernel set 1")
            self._anaconda._intf.instProgress.terminal.run_command("cd "+self._root+"/usr/src/linux")
            self._anaconda._intf.instProgress.terminal.run_command("make && make modules && make modules_install")
            shutil.copy2(self._root+"/usr/src/linux/arch/x86/boot/bzImage", self._root+"/boot/kernel-gentoo")
            
        self._progress.set_fraction(1.0)
        
    def _get_langpacks(self):
        return None

    def __get_langs(self):
        def_lang = self._anaconda.instLanguage.instLang
        def_lang = def_lang.split(".")[0] # remove .UTF-8
        def_lang_2 = def_lang.split("_")[0]
        langs = [def_lang, def_lang_2]
        return set(langs)
